<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir,
        $config->application->controllersDir.'/UserHAL',
    ]
);

/**
 * Register Files, composer autoloader
 */
$loader->registerFiles(
    [
        BASE_PATH . '/vendor/autoload.php'
    ]
);

$loader->register();
