<?php

$router = $di->getRouter();

/**
 * Cors workaround
 */

$router->add(
    '/{catch:(.*)}',
    'Index::disableCors',
    ['OPTIONS']
);

/**
 * Auth
 */
$router->add(
    '/auth/register',
    'Auth::register',
    ['POST']
)->setName('auth:register');

$router->add(
    '/auth/login',
    'Auth::login',
    ['POST']
)->setName('auth:login');

$router->add(
    '/auth/code-verifier',
    'Auth::verifyCode',
    ['POST']
)->setName('auth:code-verifier');

$router->add(
    '/auth/refresher',
    'Auth::refreshToken',
    ['POST']
)->setName('auth:refresher');

/**
 * Api
 */

$router->add(
    '/users',
    'Api::getUsers',
    ['GET']
)->setName('api:users');

$router->add(
    '/users/{userId}',
    'Api::getUser',
    ['GET']
)->setName('api:user');

$router->add(
    '/users/{userId}',
    'Api::updateUser',
    ['PUT']
);


$router->add(
    '/users/{userId}/dialogs',
    'Api::getDialogs',
    ['GET']
)->setName('api:dialogs');

$router->add(
    '/users/{userId}/dialogs/{dialogId}',
    'Api::getDialog',
    ['GET']
)->setName('api:dialog');

$router->add(
    '/users/{userId}/dialogs',
    'Api::createDialog',
    ['POST']
);

$router->add(
    '/users/{userId}/dialogs/{dialogId}',
    'Api::deleteDialog',
    ['DELETE']
);

$router->add(
    '/users/{userId}/dialogs/{dialogId}',
    'Api::updateDialog',
    ['PUT']
);


$router->add(
    '/users/{userId}/dialogs/{dialogId}/messages',
    'Api::getMessages',
    ['GET']
)->setName('api:messages');

$router->add(
    '/users/{userId}/dialogs/{dialogId}/messages',
    'Api::createMessage',
    ['POST']
);

$router->add(
    '/users/{userId]/dialogs/{dialogId}/messages',
    'Api::createMessage',
    ['POST']
);

$router->add(
    '/users/{userId}/dialogs/{dialogId}/messages/{messageId}',
    'Api::getMessage',
    ['GET']
)->setName('api:message');

$router->handle($_SERVER['REQUEST_URI']);
