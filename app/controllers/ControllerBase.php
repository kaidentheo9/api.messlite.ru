<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Messages\Messages;

class ControllerBase extends Controller
{
    protected function sendJson(array $data) : Response
    {
        $this->view->disable();
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data, JSON_UNESCAPED_UNICODE));
        return $this->response;
    }

    protected function sendJsonHttpError(int $code, array $data = []) : Response
    {
        $this->response->setStatusCode($code);
        if ($data) {
            return $this->sendJson($data);
        }   
        return $this->sendEmptyJson();
    }

    protected function sendEmptyJson() : Response
    {
        $this->view->disable();
        $this->response->setContentType('application/json', 'UTF-8');
        return $this->response;
    }

    protected function sendJsonValidationErrors(Messages $messages) : Response
    {
        $data = [
            'messages' => []
        ];
        foreach ($messages as $message) {
            $data['messages'][] = (string) $message;
        }
        return $this->sendJsonHttpError(422, $data);
    }

    protected function getBody() : array
    {
        $rawBody = file_get_contents('php://input');
        return json_decode($rawBody, true) ?? [];
    }

    protected function getProtocol() : string
    {
        // Stub
        return 'http://';
    }

    protected function getDomain() : string
    {
        // Stub
        return 'api.messlite.ru.local';
    }
}
