<?php
declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Di;

use Phalcon\Validation;
use Phalcon\Validation\Validator\StringLength as StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use \Phalcon\Validation\Validator\Callback as CallbackValidator;

use MongoDB\Model\BSONArray;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Timestamp;

class ApiController extends ControllerBase
{
    public function getUsersAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $limit = (int) $request->getQuery('limit', null, 30);
        $offset = (int) $request->getQuery('offset', null, 0);
        if ($limit > 100) {
            $limit = 100;
        }

        $total = Users::count([]);

        $users = Users::find([], [
            'limit' => $limit,
            'skip' => $offset 
            ]);

        $preparedUsers = $this->getPreparedUsers($users);

        $url = $di->getShared('url');

        $links = [];

        $protocol = $this->getProtocol();
        $domain = $this->getDomain();
        $urlBase = $protocol.$domain;

        $selfLink = $urlBase.$url->getStatic([
            'for' => 'api:users',
        ]);
        $links['self'] = [
            'href' => $selfLink.'?limit='.$limit.'&offset='.$offset
        ];

        if ($offset > 0) {
            $prevOffset = $offset - $limit;
            $prevOffset = $prevOffset > 0 ? $prevOffset : 0;
            $links['prev'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$prevOffset
            ];
        }

        if (($limit + $offset) < $total) {
            $nextOffset = $limit + $offset;
            $links['next'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$nextOffset
            ];
        }

        $data = [
            '_limit' => $limit,
            '_offset' => $offset,
            '_total' => $total,
            '_links' => $links,
            '_embedded' => [
                'items' => $preparedUsers
            ]
        ];
        return $this->sendJson($data);
    }

    private function getPreparedUsers(array $users) : array
    {
        $data = [];
        foreach ($users as $user) {
            $data[] = $this->getPreparedUser($user);
        }
        return $data;
    }

    private function getPreparedUser(Users $user) : array
    {
        $request = new Request();
        $token = $request->getQuery('token');
        
        $di = Di::getDefault();
        $url = $di->getShared('url');
        $urlBase = $this->getProtocol().$this->getDomain();
        $selfLink = $urlBase.$url->get([
            'for' => 'api:user',
            'userId' => $user->getId()
        ]);
        $data = [
            'id' => $user->getId(),
            'login' => $user->login,
            '_links' => [
                'self' => [
                    'href' => $selfLink
                ]
            ]
        ];
        return $data;
    }

    public function getUserAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        $user = Users::findOneById($userId);
        if (!$user) {
            return $this->sendJsonHttpError(404);
        }
        $data = [];
        if ($decodedToken['userId'] === $userId) {
            $data = $this->getFullPreparedUser($user);
        } else {
            $data = $this->getPreparedUser($user);
        }
        return $this->sendJson($data);
    }

    private function getFullPreparedUser(Users $user) : array
    {
        $request = new Request();
        $token = $request->getQuery('token');

        $di = Di::getDefault();
        $url = $di->getShared('url');
        $urlBase = $this->getProtocol().$this->getDomain();
        $selfLink = $urlBase.$url->get([
            'for' => 'api:user',
            'userId' => $user->getId()
        ]);
        $dialogsLink = $urlBase.$url->get([
            'for' => 'api:dialogs',
            'userId' => $user->getId()
        ]);
        $data = [
            'id' => $user->getId(),
            'login' => $user->login,
            'phone' => $user->phone,
            '_links' => [
                'self' => [
                    'href' => $selfLink
                ],
                'dialogs' => [
                    'href' => $dialogsLink
                ]
            ]
        ];
        return $data;
    }

    public function updateUserAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $user = Users::findOneById($userId);
        if (!$user) {
            return $this->sendJsonHttpError(404);
        }

        $body = $this->getBody();

        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }
        $validation = new Validation();
        $validation->add(
            'login',
            new StringLength(
                [
                    'max' => 30,
                    'min' => 5,
                    'messageMaximum' => 'We don\'t like really long logins',
                    'messageMinimum' => 'We want more than just their initials',
                    'includeMaximum' => true,
                    'includeMinimum' => false,
                ]
            )
        );

        $validation->add(
            'phone',
            new RegexValidator(
                [
                    'pattern' => '/^7([0-9]){10}$/',
                    'message' => 'wrong phone number'
                ]
            )
        );

        $errorMessages = $validation->validate($body);
        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $user->phone = $body['phone'];
        $user->login = $body['login'];

        if (!$user->update()) {
            return $this->sendJsonHttpError(500, [
                'messages' => ['unable to save user']
            ]);
        }

        return $this->sendJson($this->getFullPreparedUser($user));
    }

    public function getDialogsAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $limit = (int) $request->getQuery('limit', null, 30);
        $offset = (int) $request->getQuery('offset', null, 0);
        if ($limit > 100) {
            $limit = 100;
        }

        $total = Dialogs::count([
            'members' => [
                '$in' => [
                    $userId
                ]
            ]
        ]);

        $dialogs = Dialogs::find([
            'members' => [
                '$in' => [
                    $userId
                ]
            ]
        ], [
            'limit' => $limit,
            'skip' => $offset 
        ]);

        $preparedDialogs = $this->getPreparedDialogs($dialogs, $userId);

        $url = $di->getShared('url');

        $links = [];

        $protocol = $this->getProtocol();
        $domain = $this->getDomain();
        $urlBase = $protocol.$domain;

        $selfLink = $urlBase.$url->getStatic([
            'for' => 'api:dialogs',
            'userId' => $userId
        ]);
        $links['self'] = [
            'href' => $selfLink.'?limit='.$limit.'&offset='.$offset
        ];

        if ($offset > 0) {
            $prevOffset = $offset - $limit;
            $prevOffset = $prevOffset > 0 ? $prevOffset : 0;
            $links['prev'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$prevOffset
            ];
        }

        if (($limit + $offset) < $total) {
            $nextOffset = $limit + $offset;
            $links['next'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$nextOffset
            ];
        }

        $data = [
            '_limit' => $limit,
            '_offset' => $offset,
            '_total' => $total,
            '_links' => $links,
            '_embedded' => [
                'items' => $preparedDialogs
            ]
        ];
        return $this->sendJson($data);
    }

    private function getPreparedDialogs(array $dialogs, string $userId) : array
    {
        $data = [];
        foreach ($dialogs as $dialog) {
            $data[] = $this->getPreparedDialog($dialog, $userId);
        }
        return $data;
    }

    private function getPreparedDialog(Dialogs $dialog, string $userId) : array
    {
        $request = new Request();
        $token = $request->getQuery('token');
        
        $di = Di::getDefault();
        $url = $di->getShared('url');
        $urlBase = $this->getProtocol().$this->getDomain();
        $selfLink = $urlBase.$url->get([
            'for' => 'api:dialog',
            'userId' => $userId,
            'dialogId' => $dialog->getId(),
        ]);
        $data = [
            'id' => $dialog->getId(),
            'name' => $dialog->name,
            'members' => $dialog->members,
            'lastMessage' => $this->getDialogLastMessage($dialog, $userId),
            '_links' => [
                'self' => [
                    'href' => $selfLink
                ]
            ]
        ];
        return $data;
    }

    public function getDialogAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');
        $dialog = Dialogs::findOneById($dialogId);
        if (!$dialog) {
            return $this->sendJsonHttpError(404);
        }

        if (!in_array($userId, (array) $dialog->members)) {
            return $this->sendJsonHttpError(403);
        }
        return $this->sendJson($this->getPreparedDialog($dialog, $userId));
    }

    public function createDialogAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $body = $this->getBody();

        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();
        $validation->add(
            'name',
            new StringLength(
                [
                    'max' => 30,
                    'min' => 3,
                    'messageMaximum' => 'We don\'t like really long names',
                    'messageMinimum' => 'We want more than just their initials',
                    'includeMaximum' => true,
                    'includeMinimum' => true,
                ]
            )
        );

        $validation->add(
            'members',
            new CallbackValidator(
                [
                    'callback' => function (array $data) {
                        if (!isset($data['members'])) {
                            return false;
                        }
                        $members = $data['members'];
                        if (!$members) {
                            return false;
                        }
                        foreach ($members as $memberId) {
                            $member = Users::findOneById($memberId);
                            if (!$member) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'message' => 'invalid members list'
                ]
            )
        );

        $errorMessages = $validation->validate($body);
        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $dialog = new Dialogs([
            'name' => $body['name'],
            'members' => new BSONArray($body['members']),
        ]);

        $insertedDialogId = $dialog->insert();

        if (!$insertedDialogId) {
            return $this->sendJsonHttpError(500, [
                'messages' => ['unable to create new dialog']
            ]);
        }

        $dialog = Dialogs::findOneById((string) $insertedDialogId);

        return $this->sendJson($this->getPreparedDialog($dialog, $userId));
    }

    public function updateDialogAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');
        $dialog = Dialogs::findOneById($dialogId);
        if (!$dialog) {
            return $this->sendJsonHttpError(404);
        }

        if (!in_array($userId, (array) $dialog->members)) {
            return $this->sendJsonHttpError(403);
        }

        $body = $this->getBody();

        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();
        $validation->add(
            'name',
            new StringLength(
                [
                    'max' => 30,
                    'min' => 3,
                    'messageMaximum' => 'We don\'t like really long names',
                    'messageMinimum' => 'We want more than just their initials',
                    'includeMaximum' => true,
                    'includeMinimum' => true,
                ]
            )
        );

        $errorMessages = $validation->validate($body);
        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $dialog->name = $body['name'];

        if (!$dialog->update()) {
            return $this->sendJsonHttpError(500, [
                'messages' => 'unable to update dialog'
            ]);
        }

        return $this->sendJson($this->getPreparedDialog($dialog, $userId));
    }

    public function deleteDialogAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');
        $dialog = Dialogs::findOneById($dialogId);
        if (!$dialog) {
            return $this->sendJsonHttpError(404);
        }

        if (!in_array($userId, (array) $dialog->members)) {
            return $this->sendJsonHttpError(403);
        }

        $dialogMembers = (array) $dialog->members;
        $reversedMembers = array_flip($dialogMembers);
        unset($reversedMembers[$userId]);
        $dialog->members = new BSONArray(array_flip($reversedMembers));
        if (!count($dialog->members)) {
            if (!$dialog->delete()) {
                return $this->sendJsonHttpError(500, [
                    'messages' => ['unable to delete dialog']
                ]);
            }
            return $this->sendEmptyJson();
        }
        if (!$dialog->update()) {
            return $this->sendJsonHttpError(500, [
                'messages' => ['unable to delete dialog']
            ]);
        }
        return $this->sendEmptyJson();
    }

    private function getDialogLastMessage(Dialogs $dialog, string $userId) : ?array
    {
        $message = Messages::findOne([
            'dialogId' => $dialog->getId()
        ]);
        if (!$message) {
            return null;
        }
        return $this->getPreparedMessage($message, $userId);
    }

    public function getMessagesAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');

        $isDialogExists = Dialogs::findOne([
            '$and' => [
                [
                    '_id' => new ObjectId($dialogId)
                ], [
                    'members' => [
                        '$in' => [
                            $userId
                        ]
                    ]
                ]
            ]
        ]);

        if (!$isDialogExists) {
            return $this->sendJsonHttpError(404);
        }

        $limit = (int) $request->getQuery('limit', null, 30);
        $offset = (int) $request->getQuery('offset', null, 0);
        if ($limit > 100) {
            $limit = 100;
        }

        $total = Messages::count([
            'dialogId' => $dialogId
        ]);

        $messages = Messages::find([
            'dialogId' => $dialogId
        ], [
            'limit' => $limit,
            'skip' => $offset 
        ]);

        $preparedMessages = $this->getPreparedMessages($messages, $dialogId, $userid);

        $url = $di->getShared('url');

        $links = [];

        $protocol = $this->getProtocol();
        $domain = $this->getDomain();
        $urlBase = $protocol.$domain;

        $selfLink = $urlBase.$url->getStatic([
            'for' => 'api:messages',
            'userId' => $userId,
            'dialogId' => $dialogId
        ]);
        $links['self'] = [
            'href' => $selfLink.'?limit='.$limit.'&offset='.$offset
        ];

        if ($offset > 0) {
            $prevOffset = $offset - $limit;
            $prevOffset = $prevOffset > 0 ? $prevOffset : 0;
            $links['prev'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$prevOffset
            ];
        }

        if (($limit + $offset) < $total) {
            $nextOffset = $limit + $offset;
            $links['next'] = [
                'href' => $selfLink.'?limit='.$limit.'&offset='.$nextOffset
            ];
        }

        $data = [
            '_limit' => $limit,
            '_offset' => $offset,
            '_total' => $total,
            '_links' => $links,
            '_embedded' => [
                'items' => $preparedMessages
            ]
        ];
        return $this->sendJson($data);
    }

    private function getPreparedMessages(array $messages, string $userId) {
        $items = [];
        foreach ($messages as $message) {
            $items[] = $this->getPreparedMessage($message, $userId);
        }
        return $items;
    }

    private function getPreparedMessage(Messages $message, string $userId) : array
    {
        $request = new Request();
        $token = $request->getQuery('token');
        
        $di = Di::getDefault();
        $url = $di->getShared('url');
        $urlBase = $this->getProtocol().$this->getDomain();
        $selfLink = $urlBase.$url->get([
            'for' => 'api:message',
            'userId' => $userId,
            'dialogId' => $message->dialogId,
            'messageId' => $message->getId()
        ]);
        $data = [
            'id' => $message->getId(),
            'dialogId' => $message->dialogId,
            'from' => $message->from,
            'dateTime' => $message->dateTime->getTimestamp(),
            'seenBy' => $message->seenBy,
            '_links' => [
                'self' => [
                    'href' => $selfLink
                ]
            ]
        ];
        return $data;
    }

    public function getMessageAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');
        $dialog = Dialogs::findOneById($dialogId);
        if (!$dialog) {
            return $this->sendJsonHttpError(404);
        }

        if (!in_array($userId, (array) $dialog->members)) {
            return $this->sendJsonHttpError(403);
        }

        $messageId = $this->dispatcher->getParam('messageId');

        $message = Messages::findOne([
            '$and' => [
                [
                    'dialogId' => $dialogId,
                ],
                [
                    '_id' => new ObjectId($messageId)
                ]
            ]
        ]);

        if (!$message) {
            return $this->sendJsonHttpError(404);
        }

        return $this->sendJson($this->getPreparedMessage($message, $userId));
    }

    public function createMessageAction() : Response
    {
        $request = new Request();

        $token = $request->getQuery('token');

        if (!$token) {
            return $this->sendJsonHttpError(401);
        }

        $di = Di::getDefault();

        $jwtAuth = $di->getShared('jwtauth');
        $decodedToken = $jwtAuth->decodeAccess($token);

        if (!isset($decodedToken['userId'])) {
            return $this->sendJsonHttpError(401);
        }

        $userId = $this->dispatcher->getParam('userId');

        if ($userId !== $decodedToken['userId']) {
            return $this->sendJsonHttpError(403);
        }

        $dialogId = $this->dispatcher->getParam('dialogId');

        $isDialogExists = Dialogs::count([
            '$and' => [
                [
                    '_id' => new ObjectId($dialogId)
                ],
                [
                    'members' => [
                        '$in' => [
                            $userId
                        ]
                    ]
                ]
            ]
        ]);

        if (!$isDialogExists) {
            return $this->sendJsonHttpError(404);
        }

        $body = $this->getBody();

        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();
        $validation->add(
            'content',
            new StringLength(
                [
                    'max' => 3000,
                    'min' => 1,
                    'messageMaximum' => 'content too long',
                    'messageMinimum' => 'empty content',
                    'includeMaximum' => true,
                    'includeMinimum' => true,
                ]
            )
        );

        $errorMessages = $validation->validate($body);
        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $message = new Messages([
            'dialogId' => $dialogId,
            'from' => $userId,
            'dateTime' => new Timestamp(1, time()),
            'content' => $body['content'],
            'seenBy' => new BSONArray([
                $userId
            ])
        ]);

        $insertedMessageId = $message->insert();

        if (!$insertedMessageId) {
            return $this->sendJsonHttpError(500, [
                'messages' => ['unable to create new message']
            ]);
        }

        $message = Messages::findOneById((string) $insertedMessageId);

        return $this->sendJson($this->getPreparedMessage($message, $userId));
    }
}

