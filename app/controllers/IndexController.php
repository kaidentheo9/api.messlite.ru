<?php declare(strict_types=1);

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        
    }

    /**
     * CORS workaround
     */
    public function disableCorsAciton() : Response
    {
        $origin = $this->request->getHeader('ORIGIN') ?? '*';

        $this->response->setHeader('Access-Control-Allow-Origin', $origin)
            ->setHeader('Access-Controll-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
            ->setHeader('Access-Controll-Allow-Headers', 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization')
            ->setHeader('Access-Control-Allow-Credentials', true);

        return $this->response;
    }
}

