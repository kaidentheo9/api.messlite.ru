<?php
declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Di;
use Firebase\JWT\JWT;

use Phalcon\Validation;
use Phalcon\Validation\Validator\StringLength as StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class AuthController extends ControllerBase
{
    public function indexAction()
    {

    }

    public function registerAction() : Response
    {
        $body = $this->getBody();
        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();
        $validation->add(
            'login',
            new StringLength(
                [
                    'max' => 30,
                    'min' => 5,
                    'messageMaximum' => 'We don\'t like really long logins',
                    'messageMinimum' => 'We want more than just their initials',
                    'includeMaximum' => true,
                    'includeMinimum' => false,
                ]
            )
        );

        $validation->add(
            'phone',
            new RegexValidator(
                [
                    'pattern' => '/^7([0-9]){10}$/',
                    'message' => 'wrong phone number'
                ]
            )
        );

        $errorMessages = $validation->validate($body);
        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $phone = (int) $body['phone'];
        $login = (string) $body['login'];
        if ($this->phoneExists($phone)) {
            return $this->sendJsonHttpError(422, [
                'messages' => ['phone already exists']
            ]);
        }
        if ($this->loginExists($login)) {
            return $this->sendJsonHttpError(422, [
                'messages' => ['login already exists']
            ]);
        }
        if (!$this->createUser($phone, $login)) {
            return $this->sendJsonHttpError(500, [
                'message' => ['unable to save user']
            ]);
        }
        return $this->sendEmptyJson();
    }

    public function loginAction() : Response
    {
        $body = $this->getBody();
        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();

        $validation->add(
            'phone',
            new RegexValidator(
                [
                    'pattern' => '/^7([0-9]){10}$/',
                    'message' => 'wrong phone number'
                ]
            )
        );

        $errorMessages = $validation->validate($body);

        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $phone = $body['phone'];
        if (!$this->phoneExists($phone)) {
            return $this->sendJsonHttpError(422, [
                'message' => 'phone does not exists'
            ]);
        }
        if (!$this->setUserVerifyCodeByPhone($phone)) {
            return $this->sendJsonHttpError(500, [
                'message' => 'unable to set verify code'
            ]);
        }
        return $this->sendEmptyJson();
    }

    private function phoneExists(int $phone) : bool
    {
        $user = Users::findOne(['phone' => $phone]);
        if (!$user) {
            return false;
        }
        unset($user);
        return true;
    }

    private function loginExists(string $login) : bool
    {
        $user = Users::findOne(['login' => $login]);
        if (!$user) {
            return false;
        }
        unset($user);
        return true;
    }

    private function createUser(int $phone, string $login) : bool
    {
        $user = new Users([
            'phone' => $phone,
            'login' => $login,
        ]);
        
        if ($user->insert()) {
            return true;
        }
        return false;
    }

    private function genVerifyCode() : int
    {
        // Stub
        return 654454;
    }

    private function setUserVerifyCodeByPhone(int $phone) : bool
    {
        $user = Users::findOne(['phone' => $phone]);
        $user->verifyCode = $this->genVerifyCode();
        if ($user->update()) {
            $this->sendSmsWithVerifycode($phone, $user->verifyCode);
            return true;
        }
        return false;
    }

    private function sendSmsWithVerifycode(int $phone, int $code) : void
    {
        // Stub
    }

    public function verifyCodeAction() : Response
    {
        $body = $this->getBody();
        if (!$body) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['empty body']
            ]);
        }

        $validation = new Validation();
        
        $validation->add(
            'code',
            new RegexValidator(
                [
                    'pattern' => '/^([0-9]){6}$/',
                    'message' => 'wrong code format'
                ]
            )
        );

        $validation->add(
            'phone',
            new RegexValidator(
                [
                    'pattern' => '/^7([0-9]){10}$/',
                    'message' => 'wrong phone number'
                ]
            )
        );
        
        $errorMessages = $validation->validate($body);

        if (count($errorMessages)) {
            return $this->sendJsonValidationErrors($errorMessages);
        }

        $phone = $body['phone'];
        $code = $body['code'];
        $user = Users::findOne(
            ['phone' => $phone ]
        );
        if (!$user) {
            return $this->sendJsonHttpError(422, [
                'message' => 'phone does not registered'
            ]);
        }
        if ($user->verifyCode !== $code) {
            return $this->sendJsonHttpError(422, [
                'message' => 'invalid verify code'
            ]);
        }
        $payload = [
            'userId' => $user->getId()
        ];
        $jwtAuth = Di::getDefault()->get('jwtauth');
        $tokens = $jwtAuth->genTokens($payload);
        $user->verifyCode = null;
        if (!$user->update()) {
            return $this->sendJsonHttpError(500, [
                'message' => 'unable to user information'
            ]);
        }
        return $this->sendJson($tokens);
    }

    public function refreshTokenAction() : Response
    {
        $body = $this->getBody();
        if (!isset($body['token'])) {
            return $this->sendJsonHttpError(400, [
                'messages' => ['token is required']
            ]);
        }
        $refresh = $body['token'];
        $jwtAuth = Di::getDefault()->get('jwtauth');
        $payload = $jwtAuth->decodeRefresh($refresh);
        if (!isset($payload['userId'])) {
            return $this->sendJsonHttpError(401, [
                'message' => 'invalid refresh token'
            ]);
        }
        $newPayload = [
            'userId' => $payload['userId']
        ];
        $tokens = $jwtAuth->genTokens($payload);
        return $this->sendJson($tokens);
    }
}