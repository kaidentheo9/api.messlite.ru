<?php declare(strict_types=1);

use MongoDB\BSON\Timestamp;
use MongoDB\Model\BSONArray;

class Messages extends ModelBase
{
    const COLLECTION_NAME = 'messages';

    public string $dialogId;
    public string $from;
    public Timestamp $dateTime;
    public string $content;
    public BSONArray $seenBy;


    protected function getModelScheme() : array
    {
        return [
            'dialogId',
            'from',
            'dateTime',
            'content',
            'seenBy'
        ];
    }
}