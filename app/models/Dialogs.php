<?php declare(strict_types=1);

use MongoDB\Model\BSONArray;

class Dialogs extends ModelBase
{
    const COLLECTION_NAME = 'dialogs';

    public string $name;
    public BSONArray $members;

    protected function getModelScheme() : array
    {
        return [
            'name',
            'members',
        ];
    }
}