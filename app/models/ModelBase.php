<?php declare(strict_types=1);

use Phalcon\Di;
use MongoDB\BSON\ObjectId;

abstract class ModelBase
{
    protected ObjectId $_id;

    public function __construct(array $data)
    {
        foreach ($data as $prop => $value) {
            if (property_exists(static::class, $prop)) {
                $this->$prop = $value;
            }
        }
    }

    public function getId() : ?string
    {
        return (string) $this->_id;
    }

    public static function findOne(?array $condition = null) : ?self
    {
        $di = Di::getDefault();
        $db = $di->getShared('mongo');
        $collectionName = static::COLLECTION_NAME;
        $collection = $db->$collectionName;
        $data = $collection->findOne($condition ?? []);
        if (!$data) {
            return null;
        }
        return new static((array) $data);
    }

    public static function find(array $filter = [], array $options = []) : array
    {
        $di = Di::getDefault();
        $db = $di->getShared('mongo');
        $collectionName = static::COLLECTION_NAME;
        $collection = $db->$collectionName;
        $cursor = $collection->find($filter, $options);
        $entities = [];
        foreach ($cursor as $data) {
            $entities[] = new static((array) $data);
        }
        return $entities;
    }

    public static function count(array $filter = [], array $options = []) : int
    {
        $di = Di::getDefault();
        $db = $di->getShared('mongo');
        $collectionName = static::COLLECTION_NAME;
        $collection = $db->$collectionName;
        $count = $collection->count($filter, $options);
        return $count;
    }

    public static function findOneById(string $id) : ?self
    {
        $objectId = new ObjectId($id);
        return self::findOne(['_id' => $objectId]);
    }

    abstract protected function getModelScheme() : array;

    public function insert()
    {
        $scheme = $this->getModelScheme();
        $data = [];
        foreach ($scheme as $field) {
            $data[$field] = $this->$field;
        }
        
        $collectionName = static::COLLECTION_NAME;
        $collection = Di::getDefault()->getShared('mongo')->$collectionName;
        $result = $collection->insertOne($data);
        return $result->getInsertedId();
    }

    public function update() : bool
    {
        $scheme = $this->getModelScheme();
        $data = [];
        foreach ($scheme as $field) {
            $data[$field] = $this->$field;
        }
        $collectionName = static::COLLECTION_NAME;
        $collection = Di::getDefault()->getShared('mongo')->$collectionName;
        $result = $collection->updateOne(
            ['_id' => $this->_id],
            ['$set' => $data]
        );
        return $result->isAcknowledged();
    }

    public function delete() : bool
    {
        $collectionName = static::COLLECTION_NAME;
        $collection = Di::getDefault()->getShared('mongo')->$collectionName;
        $result = $collection->deleteOne(['_id' => $this->_id]);
        return (bool) $result->getDeletedCount();
    }
}