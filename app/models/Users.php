<?php declare(strict_types=1);

class Users extends ModelBase
{
    const COLLECTION_NAME = 'users';

    public string $login;
    public int $phone;
    public ?int $verifyCode = null;

    protected function getModelScheme() : array
    {
        return [
            'login',
            'phone',
            'verifyCode'
        ];
    }
}