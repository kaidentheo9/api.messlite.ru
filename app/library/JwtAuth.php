<?php declare(strict_types=1);

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;

class JwtAuth
{
    private string $accessKey;
    private string $refreshKey;
    private int $accessExpirationMins;
    private int $refreshExpirationDays;

    public function __construct(
        string $accessKey,
        string $refreshKey,
        int $accessExpirationMins = 2,
        int $refreshExpirationDays = 2
    ) {
        $this->accessKey = $accessKey;
        $this->refreshKey = $refreshKey;
        $this->accessExpirationMins = $accessExpirationMins;
        $this->refreshExpirationDays = $refreshExpirationDays;
    }

    public function genTokens(array $payload) : array
    {
        return [
            'access' => $this->genAccess($payload),
            'refresh' => $this->genRefresh($payload),
        ];
    }

    public function genAccess(array $payload) : string
    {
        $exp = time() + ($this->accessExpirationMins * 60);
        return $this->genToken($payload, $this->accessKey, $exp);
    }

    public function genRefresh(array $payload) : string
    {
        $exp = time() + ($this->refreshExpirationDays * 60 * 60 * 24);
        return $this->genToken($payload, $this->refreshKey, $exp);
    }

    private function genToken(array $payload, string $key, int $exp) : string
    {
        $payload['expiration'] = $exp;
        return JWT::encode($payload, $key);
    }

    public function decodeAccess(string $access) : ?array
    {
        return $this->decodeToken($access, $this->accessKey);
    }

    public function decodeRefresh(string $refresh) : ?array
    {
        return $this->decodeToken($refresh, $this->refreshKey);
    }

    private function decodeToken(string $token, string $key) : ?array
    {
        $payload = (array) JWT::decode($token, $key, ['HS256']);
        if (!isset($payload['expiration'])) {
            return null;
        }
        if (time() > $payload['expiration']) {
            return null;
        }
        return $payload;
    }
}